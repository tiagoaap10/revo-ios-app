//
//  Results.swift
//  revo-currency-ios
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(NetworkError)
}

enum NetworkError: Error {
    
    case failedParsing
    case failedRequest
    case invalidData
    case invalidURL
}
