//
//  NetworkHandler.swift
//  revo-currency-ios
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

protocol NetworkHandler {
    
    typealias HttpResponseCompletion = (_ success: Bool, _ error: NetworkError?, _ result: Any?) -> Void
    
    func makeRequest(method: Method, url: URL, parameters: [String: Any]?, completion: @escaping HttpResponseCompletion)
    
}

struct NetworkHandlerImpl: NetworkHandler {
    
    func makeRequest(method: Method, url: URL, parameters: [String : Any]?, completion: @escaping HttpResponseCompletion) {
        
    }
}
