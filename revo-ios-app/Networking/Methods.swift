//
//  Methods.swift
//  revo-currency-ios
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

enum Methods {
    case get
    case post
    case delete
    case patch
    case put
}

