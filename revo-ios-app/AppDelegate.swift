//
//  AppDelegate.swift
//  revo-ios-app
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let viewModel = CurrenciesConverterViewModelImpl()
        let viewController = CurrenciesViewController(viewModel: viewModel)
        
        let navigationController = UINavigationController(rootViewController: viewController)
        window?.rootViewController = navigationController;
        window?.makeKeyAndVisible()
        
        return true
    }

}

