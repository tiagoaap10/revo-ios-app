//
//  CurrenciesConverterViewModel.swift
//  revo-currency-ios
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

protocol CurrenciesConverterViewModel {
    
    var title: String { get }
    
    // MARK: - Datasource
    func numberOfRowsInSection(_ section: Int) -> Int
    
}

class CurrenciesConverterViewModelImpl: CurrenciesConverterViewModel {
    
    let currenciesFile = "currencies"
    
    let currencies: [Currency]?
    
    required init() {
        
        self.currencies = CurrenciesDecoder.currencies(in: currenciesFile)
        createCellsViewModels(with: self.currencies)
    }
    
    func createCellsViewModels(with currencies: [Currency]?) {
        
        guard let currencies = currencies else {
            return
        }
        
        currencies.forEach { print($0) }
        
    }
    
    // MARK: - Protocol
    
    var title: String {
        return "Rates & converstions" // Should replace this with a localizeable string
    }
    
    // MARK: - Datasource
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return currencies?.count ?? 0
    }
}
