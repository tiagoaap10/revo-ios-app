//
//  Decoder.swift
//  revo-ios-app
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

import Foundation

struct CurrenciesDecoder {
    
    static public func currencies(in file: String) -> [Currency]? {
        
        guard
            let jsonFile = Bundle.main.path(forResource: file, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: jsonFile), options: []) else {
                print("Unable to get currencies from json file.")
                return nil
        }
        
        let jsonDecoder = JSONDecoder()
        
        do {
            let currencies = try jsonDecoder.decode([Currency].self, from: data).sorted { $0.description.localizedCaseInsensitiveCompare($1.description) == ComparisonResult.orderedAscending }
            return currencies
        }
        catch let error {
            print(error.localizedDescription)
        }
        
        return nil
    }
}
