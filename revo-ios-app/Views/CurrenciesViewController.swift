//
//  CurrenciesViewController.swift
//  revo-currency-ios
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import UIKit

class CurrenciesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: - Outlets
    @IBOutlet var tableView: UITableView!
    
    
    let viewModel: CurrenciesConverterViewModel
    
    init(viewModel: CurrenciesConverterViewModel) {
        
        self.viewModel = viewModel
        
        let bundle = Bundle.init(for: CurrenciesViewController.self)
        super.init(nibName: "CurrenciesViewController", bundle: bundle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = viewModel.title
        self.initialiseTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    func initialiseTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension CurrenciesViewController {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

extension CurrenciesViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text = "Connecting the dots"
        return cell
    }
}

