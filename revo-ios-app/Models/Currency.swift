//
//  Currency.swift
//  revo-currency-ios
//
//  Created by Tiago Pereira on 24/02/2019.
//  Copyright © 2019 Tiago Pereira. All rights reserved.
//

import Foundation

struct Currency: Codable {
    
    var code: String
    var description: String
    var icon: String
    
    public init(code: String, description: String, icon: String) {
        self.code = code
        self.description = description
        self.icon = icon
    }
    
    private enum CodingKeys: String, CodingKey {
        case code
        case description
        case icon
    }
}

extension Currency: CustomDebugStringConvertible {
    var debugDescription: String {
        return "\(code) - \(description) - \(icon)"
    }
}
